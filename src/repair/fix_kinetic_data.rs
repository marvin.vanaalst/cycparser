// This one I could definitely do at parsing time already

use crate::data::{KineticData, ParseResult};
use pyo3::prelude::*;
use std::collections::HashMap;

#[pyfunction]
pub fn fix_kinetic_data(mut res: ParseResult) -> ParseResult {
    let mut reactions = res.reactions;
    for reaction in reactions.iter_mut() {
        let mut kinetic_data = HashMap::new();
        for (k, v) in reaction.enzrxns.iter() {
            let mut v = v.clone();
            kinetic_data.insert(
                k.clone(),
                KineticData {
                    km: v
                        .entry("km".to_owned())
                        .or_insert_with(HashMap::new)
                        .clone(),
                    kcat: v
                        .entry("kcat".to_owned())
                        .or_insert_with(HashMap::new)
                        .clone(),
                },
            );
        }
        reaction.kinetic_data = kinetic_data;
    }
    res.reactions = reactions;
    res
}

impl ParseResult {
    pub fn fix_kinetic_data(self) -> Self {
        fix_kinetic_data(self)
    }
}

pub(crate) fn register(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(fix_kinetic_data, m)?)?;
    Ok(())
}
