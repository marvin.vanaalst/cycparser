use crate::data::{ParseCompound, ParseReaction, ParseResult};
use crate::repair::utils::reaction_is_good;
use itertools::{self, Itertools};
use pyo3::prelude::*;
use std::collections::HashMap;
use std::iter;

fn get_compound_variants(
    compounds: &HashMap<String, f64>,
    compound_types: &HashMap<String, Vec<String>>,
) -> HashMap<String, Vec<String>> {
    let mut variants = HashMap::new();
    for compound in compounds.keys() {
        if compound_types.contains_key(compound) {
            variants.insert(
                compound.clone(),
                compound_types.get(compound).unwrap().clone(),
            );
        }
    }
    variants
}

fn get_reaction_variant(
    reaction: &ParseReaction,
    old_compounds: Vec<String>,
    new_compounds: Vec<String>,
    substrate_variants: &HashMap<String, Vec<String>>,
    product_variants: &HashMap<String, Vec<String>>,
    count: i32,
) -> ParseReaction {
    let mut reaction = reaction.clone();
    let compound_map: HashMap<String, String> = old_compounds
        .into_iter()
        .zip(new_compounds.into_iter())
        .collect();
    for old_substrate in substrate_variants.keys() {
        let new_substrate = compound_map.get(old_substrate).unwrap();
        let value = reaction.substrates.remove(old_substrate).unwrap();
        let compartment = reaction
            .substrate_compartments
            .remove(old_substrate)
            .unwrap();
        reaction.substrates.insert(new_substrate.to_owned(), value);
        reaction
            .substrate_compartments
            .insert(new_substrate.to_owned(), compartment);
    }
    for old_product in product_variants.keys() {
        let new_product = compound_map.get(old_product).unwrap();
        let value = reaction.products.remove(old_product).unwrap();
        let compartment = reaction.product_compartments.remove(old_product).unwrap();
        reaction.products.insert(new_product.to_owned(), value);
        reaction
            .product_compartments
            .insert(new_product.to_owned(), compartment);
    }
    reaction.id = format!("{}__var__{}", reaction.id, count);
    reaction.var = Some(count);
    reaction
}

fn gather_compound_types(
    compounds: &HashMap<String, ParseCompound>,
) -> HashMap<String, Vec<String>> {
    let mut types: HashMap<String, Vec<String>> = HashMap::new();
    for (id, compound) in compounds {
        match compound.types.iter().rev().next() {
            Some(compound_type) => {
                types
                    .entry(format!("{}_c", compound_type))
                    .or_insert_with(Vec::new)
                    .push(id.clone());
            }
            None => continue,
        }
    }
    types
}

#[pyfunction]
pub fn fix_create_reaction_variants(mut res: ParseResult) -> ParseResult {
    let mut new_reactions: Vec<ParseReaction> = Vec::new();
    let compounds = &res.compounds;
    let compound_types: HashMap<String, Vec<String>> = gather_compound_types(compounds);

    for reaction in res.reactions.into_iter() {
        let mut count = 0;
        let substrate_variants = get_compound_variants(&reaction.substrates, &compound_types);
        let product_variants = get_compound_variants(&reaction.products, &compound_types);

        let variants: HashMap<String, Vec<String>> = substrate_variants
            .clone()
            .into_iter()
            .chain(product_variants.clone())
            .collect();

        if variants.is_empty() {
            if reaction_is_good(&reaction, compounds) {
                new_reactions.push(reaction);
            }
        } else {
            for (new_compounds, old_compounds) in variants
                .clone()
                .into_iter()
                .map(|(_, v)| v)
                .multi_cartesian_product()
                .zip(iter::repeat(
                    variants.into_iter().map(|(k, _)| k).collect_vec(),
                ))
            {
                let new_reaction = get_reaction_variant(
                    &reaction,
                    old_compounds,
                    new_compounds,
                    &substrate_variants,
                    &product_variants,
                    count,
                );

                if reaction_is_good(&new_reaction, compounds) {
                    new_reactions.push(new_reaction);
                    count += 1;
                }
            }
        }
    }
    res.reactions = new_reactions;
    res
}

impl ParseResult {
    pub fn fix_create_reaction_variants(self) -> Self {
        fix_create_reaction_variants(self)
    }
}

pub(crate) fn register(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(fix_create_reaction_variants, m)?)?;
    Ok(())
}
