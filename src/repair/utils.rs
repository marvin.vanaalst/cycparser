use std::collections::{HashMap, HashSet};

use crate::data::{ParseCompound, ParseReaction};

pub fn check_compound_existence(
    reaction: &ParseReaction,
    compounds: &HashMap<String, ParseCompound>,
) -> bool {
    for compound in reaction.substrates.keys().chain(reaction.products.keys()) {
        if !compounds.contains_key(compound) {
            return false;
        }
    }
    true
}

pub fn check_mass_balance(
    reaction: &ParseReaction,
    compounds: &HashMap<String, ParseCompound>,
) -> bool {
    let lhs = reaction.substrates.clone();
    let rhs = reaction.products.clone();

    let mut lhs_atoms: HashMap<String, f64> = HashMap::new();
    let mut rhs_atoms: HashMap<String, f64> = HashMap::new();

    for (compound_id, stoichiometry) in lhs.iter() {
        match compounds.get(compound_id) {
            Some(compound) => {
                if compound.formula.is_empty() {
                    return false;
                }
                for (atom, count) in compound.formula.clone() {
                    *lhs_atoms.entry(atom).or_insert(0.0) -= count * stoichiometry;
                }
            }
            None => return false,
        }
    }
    for (compound, stoichiometry) in rhs.iter() {
        match compounds.get(compound) {
            Some(compound) => {
                if compound.formula.is_empty() {
                    return false;
                }
                for (atom, count) in compound.formula.clone() {
                    *rhs_atoms.entry(atom).or_insert(0.0) += count * stoichiometry;
                }
            }
            None => return false,
        }
    }

    let all_atoms: HashSet<&String> = lhs_atoms.keys().chain(rhs_atoms.keys()).collect();
    for key in all_atoms {
        if lhs_atoms.get(key).unwrap_or(&0.0) - rhs_atoms.get(key).unwrap_or(&0.0) != 0.0 {
            return false;
        }
    }
    true
}

pub fn check_charge_balance(
    reaction: &ParseReaction,
    compounds: &HashMap<String, ParseCompound>,
) -> bool {
    let mut lhs_charge = 0.0;
    let mut rhs_charge = 0.0;
    for (compound_id, stoichiometry) in reaction.substrates.iter() {
        match compounds.get(compound_id) {
            Some(compound) => lhs_charge -= stoichiometry * compound.charge,
            None => return false,
        }
    }
    for (compound_id, stoichiometry) in reaction.products.iter() {
        match compounds.get(compound_id) {
            Some(compound) => rhs_charge += stoichiometry * compound.charge,
            None => return false,
        }
    }
    lhs_charge - rhs_charge == 0.0
}

pub fn reaction_is_good(
    reaction: &ParseReaction,
    compounds: &HashMap<String, ParseCompound>,
) -> bool {
    check_compound_existence(reaction, compounds)
        & check_mass_balance(reaction, compounds)
        & check_charge_balance(reaction, compounds)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::data::{ParseCompound, ParseReaction};

    #[test]
    fn test_check_compound_existence() {
        let reaction = ParseReaction {
            substrates: hashmap! {"CPD1".to_owned() => -1.0},
            products: hashmap! {"CPD2".to_owned() => 1.0},
            ..Default::default()
        };
        let compounds = hashmap! {
            "CPD1".to_owned() => ParseCompound {..Default::default()},
            "CPD2".to_owned() => ParseCompound {..Default::default()},
        };
        assert!(check_compound_existence(&reaction, &compounds));
    }

    #[test]
    fn test_check_compound_existence_left_missing() {
        let reaction = ParseReaction {
            substrates: hashmap! {"CPD1".to_owned() => -1.0},
            products: hashmap! {"CPD2".to_owned() => 1.0},
            ..Default::default()
        };
        let compounds = hashmap! {
            "CPD2".to_owned() => ParseCompound {..Default::default()},
        };
        assert!(!check_compound_existence(&reaction, &compounds));
    }

    #[test]
    fn test_check_compound_existence_righ_missing() {
        let reaction = ParseReaction {
            substrates: hashmap! {"CPD1".to_owned() => -1.0},
            products: hashmap! {"CPD2".to_owned() => 1.0},
            ..Default::default()
        };
        let compounds = hashmap! {
            "CPD1".to_owned() => ParseCompound {..Default::default()},
        };
        assert!(!check_compound_existence(&reaction, &compounds));
    }

    #[test]
    fn test_check_mass_balance() {
        let reaction = ParseReaction {
            substrates: hashmap! {"CPD1".to_owned() => -1.0},
            products: hashmap! {"CPD2".to_owned() => 1.0},
            ..Default::default()
        };
        let compounds = hashmap! {
            "CPD1".to_owned() => ParseCompound {
                formula: hashmap!{"C".to_owned() => 1.0},
                ..Default::default()
            },
            "CPD2".to_owned() => ParseCompound {
                formula: hashmap!{"C".to_owned() => 1.0},
                ..Default::default()
            },
        };
        assert!(check_mass_balance(&reaction, &compounds));
    }

    #[test]
    fn test_check_mass_balance_left_missing() {
        let reaction = ParseReaction {
            substrates: hashmap! {"CPD1".to_owned() => -1.0},
            products: hashmap! {"CPD2".to_owned() => 1.0},
            ..Default::default()
        };
        let compounds = hashmap! {
            "CPD1".to_owned() => ParseCompound {
                ..Default::default()
            },
            "CPD2".to_owned() => ParseCompound {
                formula: hashmap!{"C".to_owned() => 1.0},
                ..Default::default()
            },
        };
        assert!(!check_mass_balance(&reaction, &compounds));
    }

    #[test]
    fn test_check_mass_balance_right_missing() {
        let reaction = ParseReaction {
            substrates: hashmap! {"CPD1".to_owned() => -1.0},
            products: hashmap! {"CPD2".to_owned() => 1.0},
            ..Default::default()
        };
        let compounds = hashmap! {
            "CPD1".to_owned() => ParseCompound {
                formula: hashmap!{"C".to_owned() => 1.0},
                ..Default::default()
            },
            "CPD2".to_owned() => ParseCompound {
                ..Default::default()
            },
        };
        assert!(!check_mass_balance(&reaction, &compounds));
    }

    #[test]
    fn test_charge_balance_balanced() {
        let reaction = ParseReaction {
            substrates: hashmap! {"CPD1".to_owned() => -1.0},
            products: hashmap! {"CPD2".to_owned() => 1.0},
            ..Default::default()
        };
        let compounds = hashmap! {
            "CPD1".to_owned() => ParseCompound {charge: 1.0, ..Default::default()},
            "CPD2".to_owned() => ParseCompound {charge: 1.0, ..Default::default()},
        };
        assert!(check_charge_balance(&reaction, &compounds));
    }

    #[test]
    fn test_charge_balance_left_missing() {
        let reaction = ParseReaction {
            substrates: hashmap! {"CPD1".to_owned() => -1.0},
            products: hashmap! {"CPD2".to_owned() => 1.0},
            ..Default::default()
        };
        let compounds = hashmap! {
            "CPD1".to_owned() => ParseCompound {..Default::default()},
            "CPD2".to_owned() => ParseCompound {charge: 1.0, ..Default::default()},
        };
        assert!(!check_charge_balance(&reaction, &compounds));
    }

    #[test]
    fn test_charge_balance_right_missing() {
        let reaction = ParseReaction {
            substrates: hashmap! {"CPD1".to_owned() => -1.0},
            products: hashmap! {"CPD2".to_owned() => 1.0},
            ..Default::default()
        };
        let compounds = hashmap! {
            "CPD1".to_owned() => ParseCompound {charge: 1.0, ..Default::default()},
            "CPD2".to_owned() => ParseCompound {..Default::default()},
        };
        assert!(!check_charge_balance(&reaction, &compounds));
    }
}
