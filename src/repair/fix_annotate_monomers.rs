use crate::data::ParseResult;
use pyo3::prelude::*;
use std::collections::HashMap;

#[pyfunction]
pub fn fix_annotate_monomers(mut parse_result: ParseResult) -> ParseResult {
    let mut reactions = parse_result.reactions;
    let monomers = parse_result.genes;
    for reaction in reactions.iter_mut() {
        for (k, rnx_monomers) in reaction.monomers.iter() {
            let mut annotated_monomers = HashMap::new();
            for monomer in rnx_monomers {
                if let Some(v) = monomers.get(monomer) {
                    annotated_monomers.insert(monomer.clone(), v.clone());
                }
            }
            reaction
                .monomers_annotated
                .insert(k.to_owned(), annotated_monomers);
        }
    }
    parse_result.reactions = reactions;
    parse_result.genes = monomers;
    parse_result
}

impl ParseResult {
    pub fn fix_annotate_monomers(self) -> Self {
        fix_annotate_monomers(self)
    }
}

pub(crate) fn register(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(fix_annotate_monomers, m)?)?;
    Ok(())
}
