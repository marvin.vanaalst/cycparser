use crate::data::{ParseCompound, ParseReaction, ParseResult};
use pyo3::prelude::*;
use regex::Regex;
use std::collections::HashMap;

fn map_compartment_to_model_compartments(
    compartment: &str,
    compartment_map: &HashMap<String, String>,
) -> String {
    match compartment_map.get(compartment) {
        Some(x) => x.to_owned(),
        None => compartment_map.get("CYTOSOL").unwrap().to_owned(),
    }
}

fn add_moped_compartment_suffix(
    object_id: &str,
    compartment: &str,
    compartment_suffixes: &HashMap<String, String>,
) -> String {
    format!(
        "{}_{}",
        object_id,
        compartment_suffixes.get(compartment).unwrap().to_owned()
    )
}

fn split_location(
    location: &str,
    compartment_map: &HashMap<String, String>,
) -> HashMap<String, String> {
    let regex = Regex::new("-CCO-").unwrap();
    let mut split = regex.split(location);
    hashmap! {
        "CCO-IN".to_owned() => map_compartment_to_model_compartments(split.next().unwrap_or("CYTOSOL"), compartment_map),
        "CCO-OUT".to_owned() => map_compartment_to_model_compartments(split.next().unwrap(), compartment_map),
    }
}

fn add_compartment_compound_variant(
    compound_id: &str,
    compartment: &str,
    compounds: &mut HashMap<String, ParseCompound>,
    compartment_suffixes: &HashMap<String, String>,
) -> String {
    let new_id = add_moped_compartment_suffix(
        compounds.get(compound_id).unwrap().base_id.as_str(),
        compartment,
        compartment_suffixes,
    );
    let mut new_compound = compounds.get(compound_id).unwrap().clone();
    new_compound.compartment = Some(compartment.to_owned());
    compounds.insert(new_id.clone(), new_compound);
    new_id
}

fn all_compartments_match(reaction: &ParseReaction, compartment: &str) -> bool {
    reaction
        .substrate_compartments
        .values()
        .chain(reaction.product_compartments.values())
        .all(|x| x == compartment)
}

fn create_compartment_reaction(
    reaction: &ParseReaction,
    compartment: &str,
    compounds: &mut HashMap<String, ParseCompound>,
    compartment_suffixes: &HashMap<String, String>,
) -> ParseReaction {
    let mut reaction = reaction.clone();
    for compound_id in reaction.substrate_compartments.keys() {
        let new_compound_id = add_compartment_compound_variant(
            compound_id,
            compartment,
            compounds,
            compartment_suffixes,
        );
        let old = reaction.substrates.remove(compound_id).unwrap();
        reaction.substrates.insert(new_compound_id, old);
    }
    for compound_id in reaction.product_compartments.keys() {
        let new_compound_id = add_compartment_compound_variant(
            compound_id,
            compartment,
            compounds,
            compartment_suffixes,
        );
        let old = reaction.products.remove(compound_id).unwrap();
        reaction.products.insert(new_compound_id, old);
    }
    reaction.id = add_moped_compartment_suffix(&reaction.id, compartment, compartment_suffixes);
    reaction.compartment = compartment.to_owned();
    reaction
}

fn get_sides(
    side: &str,
    location: &str,
    compartment_map: &HashMap<String, String>,
) -> HashMap<String, String> {
    if location.contains("-CCO-") {
        split_location(location, compartment_map)
    } else {
        hashmap! {side.to_owned() => map_compartment_to_model_compartments(&location[4..].to_owned(), compartment_map)}
    }
}
fn create_single_compartment_variant(
    reaction: &ParseReaction,
    location: &str,
    side: &str,
    compounds: &mut HashMap<String, ParseCompound>,
    compartment_map: &HashMap<String, String>,
    compartment_suffixes: &HashMap<String, String>,
) -> ParseReaction {
    let sides = get_sides(side, location, compartment_map);
    create_compartment_reaction(
        reaction,
        sides.get(side).unwrap(),
        compounds,
        compartment_suffixes,
    )
}

fn create_transmembrane_reaction(
    reaction: ParseReaction,
    sides: HashMap<String, String>,
    compounds: &mut HashMap<String, ParseCompound>,
    compartment_suffixes: &HashMap<String, String>,
) -> ParseReaction {
    let mut reaction = reaction;
    for (compound_id, side) in reaction.substrate_compartments.iter() {
        let new_compound_id = add_compartment_compound_variant(
            compound_id,
            sides.get(side).unwrap(),
            compounds,
            compartment_suffixes,
        );
        let old = reaction.substrates.remove(compound_id).unwrap();
        reaction.substrates.insert(new_compound_id, old);
    }
    for (compound_id, side) in reaction.product_compartments.iter() {
        let new_compound_id = add_compartment_compound_variant(
            compound_id,
            sides.get(side).unwrap(),
            compounds,
            compartment_suffixes,
        );
        let old = reaction.products.remove(compound_id).unwrap();
        reaction.products.insert(new_compound_id, old);
    }
    // Add suffix to reaction name
    let in_suffix =
        add_moped_compartment_suffix("", sides.get("CCO-IN").unwrap(), compartment_suffixes);
    let out_suffix =
        add_moped_compartment_suffix("", sides.get("CCO-OUT").unwrap(), compartment_suffixes);
    reaction.id = format!("{}{}{}", reaction.id, in_suffix, out_suffix);
    reaction.transmembrane_compartments = Some((
        sides.get("CCO-IN").unwrap().to_owned(),
        sides.get("CCO-OUT").unwrap().to_owned(),
    ));
    reaction.transmembrane = true;
    reaction
}

#[pyfunction]
pub fn fix_create_compartment_variants(
    mut res: ParseResult,
    compartment_map: HashMap<String, String>,
    compartment_suffixes: HashMap<String, String>,
) -> ParseResult {
    let mut new_reactions: Vec<ParseReaction> = Vec::new();
    let mut reactions = res.reactions;
    let mut compounds = res.compounds;

    for reaction in reactions.iter_mut() {
        if all_compartments_match(reaction, "CCO-IN") {
            if reaction.locations.is_empty() {
                reaction.locations = vec!["CCO-CYTOSOL".to_owned()];
            }
            for location in reaction.locations.iter() {
                let local = create_single_compartment_variant(
                    reaction,
                    location,
                    "CCO-IN",
                    &mut compounds,
                    &compartment_map,
                    &compartment_suffixes,
                );
                new_reactions.push(local);
            }
        } else if all_compartments_match(reaction, "CCO-OUT") {
            if reaction.locations.is_empty() {
                reaction.locations = vec!["CCO-EXTRACELLULAR".to_owned()];
            }
            for location in reaction.locations.iter() {
                let local = create_single_compartment_variant(
                    reaction,
                    location,
                    "CCO-OUT",
                    &mut compounds,
                    &compartment_map,
                    &compartment_suffixes,
                );
                new_reactions.push(local);
            }
        } else {
            if reaction.locations.is_empty() {
                reaction.locations = vec!["CCO-EXTRACELLULAR-CCO-CYTOSOL".to_owned()];
            }
            for location in reaction.locations.iter() {
                let sides = split_location(location, &compartment_map);
                if sides.get("CCO-IN").unwrap() == sides.get("CCO-OUT").unwrap() {
                    let local = create_single_compartment_variant(
                        reaction,
                        location,
                        "CCO-OUT",
                        &mut compounds,
                        &compartment_map,
                        &compartment_suffixes,
                    );
                    new_reactions.push(local);
                } else {
                    let local = create_transmembrane_reaction(
                        reaction.clone(),
                        sides,
                        &mut compounds,
                        &compartment_suffixes,
                    );
                    new_reactions.push(local);
                }
            }
        }
    }
    for reaction in new_reactions {
        reactions.push(reaction);
    }
    res.compounds = compounds;
    res.reactions = reactions;
    res
}

impl ParseResult {
    pub fn fix_create_compartment_variants(
        self,
        compartment_map: HashMap<String, String>,
        compartment_suffixes: HashMap<String, String>,
    ) -> Self {
        fix_create_compartment_variants(self, compartment_map, compartment_suffixes)
    }
}

pub(crate) fn register(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(fix_create_compartment_variants, m)?)?;
    Ok(())
}
