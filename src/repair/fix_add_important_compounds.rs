use crate::data::{ManualAddition, ParseCompound, ParseResult};
use pyo3::prelude::*;
use std::collections::HashMap;

fn transform_manual_additions(
    manual_additions: HashMap<String, ManualAddition>,
) -> HashMap<String, ParseCompound> {
    manual_additions
        .into_iter()
        .map(|(k, v)| {
            (
                k,
                ParseCompound {
                    base_id: v.base_id,
                    id: v.id,
                    charge: v.charge,
                    formula: v.formula,
                    compartment: Some(v.compartment),
                    ..Default::default()
                },
            )
        })
        .collect()
}

#[pyfunction]
pub fn fix_add_important_compounds(
    mut result: ParseResult,
    manual_additions: HashMap<String, ManualAddition>,
) -> ParseResult {
    let manual_additions = transform_manual_additions(manual_additions);
    for (k, v) in manual_additions {
        result.compounds.insert(k, v);
    }
    result
}

impl ParseResult {
    pub fn fix_add_important_compounds(
        self,
        manual_additions: HashMap<String, ManualAddition>,
    ) -> Self {
        fix_add_important_compounds(self, manual_additions)
    }
}

pub(crate) fn register(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(fix_add_important_compounds, m)?)?;
    Ok(())
}
