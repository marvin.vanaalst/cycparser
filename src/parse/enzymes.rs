use crate::data::ParseEnzyme;
use crate::utils::read_lines;
use std::collections::HashMap;

pub fn parse_enzymes(path: &str) -> HashMap<String, ParseEnzyme> {
    let mut enzymes = HashMap::new();
    let mut current_enzyme = ParseEnzyme::default();
    let mut current_id = "".to_owned();
    let mut last_identifier = "";
    let mut last_value = 0.0;

    if let Ok(lines) = read_lines(path) {
        for line in lines.into_iter().flatten() {
            if line.starts_with("//") {
                enzymes.insert(current_id.clone(), current_enzyme.clone());
                continue;
            }
            let (identifier, content) = line.split_once(" - ").unwrap_or_default();
            match identifier {
                "UNIQUE-ID" => {
                    current_id = content.to_owned();
                    current_enzyme = ParseEnzyme::default();
                }
                "ENZYME" => {
                    current_enzyme.enzyme = Some(content.to_owned());
                }
                "KCAT" => {
                    last_identifier = "KCAT";
                    last_value = content.trim().parse::<f64>().unwrap_or_default();
                }
                "KM" => {
                    last_identifier = "KM";
                    last_value = content.trim().parse::<f64>().unwrap_or_default();
                }
                "VMAX" => {
                    last_identifier = "VMAX";
                    last_value = content.trim().parse::<f64>().unwrap_or_default();
                }
                "^SUBSTRATE" => match last_identifier {
                    "KM" => {
                        current_enzyme.km.insert(content.to_owned(), last_value);
                    }
                    "VMAX" => {
                        current_enzyme.vmax.insert(content.to_owned(), last_value);
                    }
                    "KCAT" => {
                        current_enzyme.kcat.insert(content.to_owned(), last_value);
                    }
                    _ => continue,
                },
                _ => continue,
            }
        }
    } else {
        eprintln!("Could not find file {}", path);
    }
    enzymes
}
