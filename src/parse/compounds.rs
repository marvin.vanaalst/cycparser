use crate::utils::{read_lines, starts_with_ignored};
use std::collections::{HashMap, HashSet};

use crate::data::ParseCompound;

pub fn parse_compounds(path: &str) -> HashMap<String, ParseCompound> {
    let mut compounds = HashMap::new();
    let mut current_compound = ParseCompound::default();
    if let Ok(lines) = read_lines(path) {
        for line in lines.into_iter().flatten() {
            if line.starts_with("//") {
                compounds.insert(current_compound.id.clone(), current_compound.clone());
                continue;
            } else if starts_with_ignored(&line) {
                continue;
            }
            let (identifier, content) = line.split_once(" - ").unwrap_or_default();
            match identifier {
                "UNIQUE-ID" => {
                    let current_id = content;
                    current_compound = ParseCompound {
                        base_id: current_id.clone().to_owned(),
                        id: format!("{}_c", current_id),
                        ..Default::default()
                    };
                }
                "TYPES" => current_compound.types.push(content.to_owned()),
                "COMMON-NAME" => current_compound.name = Some(content.to_owned()),
                "ATOM-CHARGES" => {
                    let (_atom, charge) = &content[1..(content.len() - 1)]
                        .split_once(" ")
                        .unwrap_or_default();
                    current_compound.charge += charge.trim().parse::<f64>().unwrap_or(0.0)
                }
                "CHEMICAL-FORMULA" => {
                    let (atom, count) = &content[1..(content.len() - 1)]
                        .split_once(" ")
                        .unwrap_or_default();
                    current_compound.formula.insert(
                        atom.to_owned().to_owned(),
                        count.trim().parse::<f64>().unwrap_or(0.0),
                    );
                }
                "DBLINKS" => {
                    let mut iter = (&content[1..(content.len() - 1)]).splitn(3, ' ');
                    let database = iter.next().unwrap_or_default();
                    let id = iter.next().unwrap_or_default();
                    current_compound
                        .database_links
                        .entry(database.to_owned())
                        .or_insert_with(HashSet::new)
                        .insert((&id[1..(id.len() - 1)]).to_owned());
                }
                "GIBBS-0" => current_compound.gibbs0 = content.trim().parse::<f64>().unwrap_or(0.0),
                "SMILES" => current_compound.smiles = Some(content.to_owned()),
                _ => {
                    continue;
                }
            }
        }
    } else {
        eprintln!("Could not find file {}", path);
    }
    compounds
}
