use crate::data::Monomer;
use crate::utils::read_lines;
use std::collections::{HashMap, HashSet};

pub fn parse_genes(path: &str) -> HashMap<String, Monomer> {
    let mut genes: HashMap<String, Monomer> = HashMap::new();

    let mut current_gene = Monomer::default();
    let mut current_id = "".to_owned();

    if let Ok(lines) = read_lines(path) {
        for line in lines.into_iter().flatten() {
            if line.starts_with("//") {
                genes.insert(current_id.clone(), current_gene.clone());
                continue;
            }
            let (identifier, content) = line.split_once(" - ").unwrap_or_default();
            match identifier {
                "UNIQUE-ID" => {
                    current_id = content.to_owned();
                    current_gene = Monomer::default();
                }
                "DBLINKS" => {
                    let mut iter = (&content[1..(content.len() - 1)]).splitn(3, ' ');
                    let database = iter.next().unwrap_or_default();
                    let id = iter.next().unwrap_or_default();
                    current_gene
                        .database_links
                        .entry(database.to_owned())
                        .or_insert_with(HashSet::new)
                        .insert((&id[1..(id.len() - 1)]).to_owned());
                }
                "PRODUCT" => current_gene.gene = Some(content.to_owned()),
                _ => continue,
            }
        }
    } else {
        eprintln!("Could not find file {}", path);
    }
    genes
}
