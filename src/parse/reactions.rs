use crate::data::ParseReaction;
use crate::utils::{read_lines, starts_with_ignored};
use std::collections::HashSet;

pub fn parse_reactions(path: &str) -> Vec<ParseReaction> {
    let mut reactions: Vec<ParseReaction> = Vec::new();
    let mut current_reaction = ParseReaction::default();
    let mut last_content = "".to_owned();
    let mut last_identifier = "";

    if let Ok(lines) = read_lines(path) {
        for line in lines.into_iter().flatten() {
            if line.starts_with("//") {
                reactions.push(current_reaction.clone());
                continue;
            } else if starts_with_ignored(&line) {
                continue;
            }
            let (identifier, content) = line.split_once(" - ").unwrap_or_default();
            match identifier {
                "UNIQUE-ID" => {
                    current_reaction = ParseReaction {
                        base_id: content.to_owned(),
                        id: content.to_owned(),
                        ..Default::default()
                    };
                }
                "TYPES" => current_reaction.types.push(content.to_owned()),
                "COMMON-NAME" => current_reaction.name = Some(content.to_owned()),
                "DBLINKS" => {
                    let mut iter = (&content[1..(content.len() - 1)]).splitn(3, ' ');
                    let database = iter.next().unwrap_or_default();
                    let id = iter.next().unwrap_or_default();
                    current_reaction
                        .database_links
                        .entry(database.to_owned())
                        .or_insert_with(HashSet::new)
                        .insert((&id[1..(id.len() - 1)]).to_owned());
                }
                "EC-NUMBER" => current_reaction.ec = Some(content.to_owned()),
                "GIBBS-0" => current_reaction.gibbs0 = content.trim().parse::<i64>().unwrap_or(0),
                "IN-PATHWAY" => {
                    current_reaction.pathways.insert(content.to_owned());
                }
                "LEFT" => {
                    last_identifier = "LEFT";
                    last_content = format!("{}_c", content);
                    current_reaction
                        .substrates
                        .insert(last_content.clone(), -1.0);
                    current_reaction
                        .substrate_compartments
                        .insert(last_content.clone(), "CCO-IN".to_owned());
                }
                "RIGHT" => {
                    last_identifier = "RIGHT";
                    last_content = format!("{}_c", content);
                    current_reaction.products.insert(last_content.clone(), 1.0);
                    current_reaction
                        .product_compartments
                        .insert(last_content.clone(), "CCO-IN".to_owned());
                }
                "^COMPARTMENT" => match last_identifier {
                    "LEFT" => {
                        let compartment = match content {
                            "CCO-OUT" => "CCO-OUT",
                            "CCO-MIDDLE" => "CCO-OUT",
                            _ => "CCO-IN",
                        };
                        current_reaction
                            .substrate_compartments
                            .insert(last_content.clone(), compartment.to_owned());
                    }
                    "RIGHT" => {
                        let compartment = match content {
                            "CCO-OUT" => "CCO-OUT",
                            "CCO-MIDDLE" => "CCO-OUT",
                            _ => "CCO-IN",
                        };
                        current_reaction
                            .product_compartments
                            .insert(last_content.clone(), compartment.to_owned());
                    }
                    _ => continue,
                },
                "^COEFFICIENT" => match last_identifier {
                    "LEFT" => {
                        current_reaction.substrates.insert(
                            last_content.clone(),
                            -content.trim().parse::<f64>().unwrap_or(1.0),
                        );
                    }
                    "RIGHT" => {
                        current_reaction.products.insert(
                            last_content.clone(),
                            content.trim().parse::<f64>().unwrap_or(1.0),
                        );
                    }
                    _ => continue,
                },
                _ => {
                    continue;
                }
            }
        }
    } else {
        eprintln!("Could not find file {}", path);
    }
    reactions
}
