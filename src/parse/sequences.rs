use crate::utils::read_lines;
use itertools::Itertools;
use std::collections::HashMap;

pub fn parse_sequences(path: &str) -> HashMap<String, String> {
    let mut sequences = HashMap::new();
    if let Ok(lines) = read_lines(path) {
        for (line1, line2) in lines.into_iter().flatten().tuples() {
            let (id, _other) = line1[10..].split_once(' ').unwrap_or_default(); // >gnl|META|
            sequences.insert(id.to_owned(), line2.to_owned());
        }
    } else {
        eprintln!("Could not find file {}", path);
    }
    sequences
}
