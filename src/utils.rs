use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

pub fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

pub fn starts_with_ignored(line: &str) -> bool {
    vec!["#", "/", "COMMENT", "CITATIONS", "^CITATIONS", "SYNONYMS"]
        .iter()
        .any(|i| line.starts_with(i))
}
