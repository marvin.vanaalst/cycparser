use crate::data::{KineticData, Monomer};
use pyo3::pyclass;
use std::collections::{HashMap, HashSet};

#[pyclass]
#[derive(Debug, Clone)]
pub struct ParseReaction {
    #[pyo3(get)]
    pub base_id: String,
    #[pyo3(get)]
    pub id: String,
    #[pyo3(get)]
    pub name: Option<String>,
    #[pyo3(get)]
    pub ec: Option<String>,
    #[pyo3(get)]
    pub gibbs0: i64,
    #[pyo3(get)]
    pub types: Vec<String>,
    #[pyo3(get)]
    pub pathways: HashSet<String>,
    #[pyo3(get)]
    pub database_links: HashMap<String, HashSet<String>>,
    // temporary
    #[pyo3(get)]
    pub enzymes: HashSet<String>,
    #[pyo3(get)]
    pub direction: String,
    #[pyo3(get)]
    pub locations: Vec<String>,
    #[pyo3(get)]
    pub reversible: bool,
    #[pyo3(get)]
    pub substrates: HashMap<String, f64>,
    #[pyo3(get)]
    pub substrate_compartments: HashMap<String, String>,
    #[pyo3(get)]
    pub products: HashMap<String, f64>,
    #[pyo3(get)]
    pub product_compartments: HashMap<String, String>,
    #[pyo3(get)]
    pub monomers: HashMap<String, HashSet<String>>,
    #[pyo3(get)]
    pub enzrxns: HashMap<String, HashMap<String, HashMap<String, f64>>>,
    // filled in later
    #[pyo3(get)]
    pub bounds: (i64, i64),
    #[pyo3(get)]
    pub monomers_annotated: HashMap<String, HashMap<String, Monomer>>,
    #[pyo3(get)]
    pub sequences: HashMap<String, String>,
    #[pyo3(get)]
    pub kinetic_data: HashMap<String, KineticData>,
    #[pyo3(get)]
    pub stoichiometries: HashMap<String, f64>,
    #[pyo3(get)]
    pub transmembrane: bool,
    #[pyo3(get)]
    pub compartment: String,
    #[pyo3(get)]
    pub transmembrane_compartments: Option<(String, String)>,
    #[pyo3(get)]
    pub var: Option<i32>,
}

impl ParseReaction {
    pub fn new(base_id: String, id: String) -> Self {
        ParseReaction {
            base_id,
            id,
            ..Default::default()
        }
    }
}

impl Default for ParseReaction {
    fn default() -> Self {
        ParseReaction {
            base_id: "NO-NAME".to_owned(),
            id: "NO-NAME".to_owned(),
            name: None,
            ec: None,
            gibbs0: 0,
            direction: "LEFT-TO-RIGHT".to_owned(),
            reversible: false,
            substrates: HashMap::new(),
            substrate_compartments: HashMap::new(),
            products: HashMap::new(),
            product_compartments: HashMap::new(),
            types: Vec::new(),
            locations: Vec::new(),
            pathways: HashSet::new(),
            enzymes: HashSet::new(),
            database_links: HashMap::new(),
            // fill in later
            bounds: (0, 0),
            monomers: HashMap::new(),
            monomers_annotated: HashMap::new(),
            sequences: HashMap::new(),
            enzrxns: HashMap::new(),
            kinetic_data: HashMap::new(),
            stoichiometries: HashMap::new(),
            compartment: "CYTOSOL".to_owned(),
            transmembrane: false,
            transmembrane_compartments: None,
            var: None,
        }
    }
}
