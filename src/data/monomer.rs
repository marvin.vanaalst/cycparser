use pyo3::pyclass;
use std::collections::{HashMap, HashSet};

#[pyclass]
#[derive(Debug, Clone)]
pub struct Monomer {
    #[pyo3(get)]
    pub gene: Option<String>,
    #[pyo3(get)]
    pub database_links: HashMap<String, HashSet<String>>,
}

impl Default for Monomer {
    fn default() -> Self {
        Self {
            gene: None,
            database_links: HashMap::new(),
        }
    }
}
