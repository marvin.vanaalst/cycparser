use crate::data::{Monomer, ParseCompound, ParseEnzyme, ParseReaction};
use pyo3::pyclass;
use std::collections::{HashMap, HashSet};

#[pyclass]
#[derive(Debug, Clone)]
pub struct ParseResult {
    #[pyo3(get)]
    pub compounds: HashMap<String, ParseCompound>,
    #[pyo3(get)]
    pub enzymes: HashMap<String, ParseEnzyme>,
    #[pyo3(get)]
    pub genes: HashMap<String, Monomer>,
    #[pyo3(get)]
    pub proteins: HashMap<String, HashSet<String>>,
    #[pyo3(get)]
    pub reactions: Vec<ParseReaction>,
    #[pyo3(get)]
    pub sequences: HashMap<String, String>,
}
