use pyo3::pyclass;
use std::collections::HashMap;

#[pyclass]
#[derive(Debug, Clone)]
pub struct ParseEnzyme {
    pub enzyme: Option<String>,
    pub kcat: HashMap<String, f64>,
    pub km: HashMap<String, f64>,
    pub vmax: HashMap<String, f64>,
}

impl Default for ParseEnzyme {
    fn default() -> Self {
        Self {
            enzyme: None,
            kcat: HashMap::new(),
            km: HashMap::new(),
            vmax: HashMap::new(),
        }
    }
}
